//
//   Cartesian 2-vectors that can be added, scaled, sized, dot and vector products etc.
//
#include "Cartesian2Vector.h"
#include <math.h>
#include <iostream> // Only for error message to cerr

Cartesian2Vector Cartesian2Vector::operator-(void) {
    return Cartesian2Vector(-x, -y);
}

Cartesian2Vector & Cartesian2Vector::operator+= (Cartesian2Vector b) {
    x += b.x; y += b.y;
    return *this;
}
Cartesian2Vector operator+ (Cartesian2Vector a, Cartesian2Vector b) {
    return Cartesian2Vector(a.x + b.x, a.y + b.y);
}

Cartesian2Vector & Cartesian2Vector::operator-= (Cartesian2Vector b) {
    x -= b.x; y -= b.y;
    return *this;
}
Cartesian2Vector operator- (Cartesian2Vector a, Cartesian2Vector b) {
    return Cartesian2Vector(a.x - b.x, a.y - b.y);
}

//Outer product:
double Cartesian2Vector::operator^= (Cartesian2Vector b) {
    return x * b.y - y * b.x;
}
double operator^ (Cartesian2Vector a, Cartesian2Vector b) {
    return a.x * b.y - a.y * b.x;
}

// Inner product
double operator* (Cartesian2Vector a, Cartesian2Vector b) {
    return a.x * b.x + a.y * b.y;
}

// Scaling
Cartesian2Vector & Cartesian2Vector::operator*= (double scalefactor) {
    x *= scalefactor; y *= scalefactor;
    return *this;
}
Cartesian2Vector operator* (Cartesian2Vector a, double scalefactor) {
    return Cartesian2Vector(a.x * scalefactor, a.y * scalefactor);
}
Cartesian2Vector operator* (double scalefactor, Cartesian2Vector a) {
    return Cartesian2Vector(a.x * scalefactor, a.y * scalefactor);
}

// Division by scalar (only post-)
Cartesian2Vector & Cartesian2Vector::operator/= (double scalefactor) {
    x /= scalefactor; y /= scalefactor;
    return *this;
}
Cartesian2Vector operator/ (Cartesian2Vector a, double scalefactor) {
    return Cartesian2Vector(a.x / scalefactor, a.y / scalefactor);
}

double Cartesian2Vector::squareModulus (void) const {
    return x * x + y * y;
}

double Cartesian2Vector::modulus (void) const {
    return sqrt(x * x + y * y);
}

Cartesian2Vector & Cartesian2Vector::rotate(Cartesian2Vector point, double phi) {
// Rotate this point about a point

    *this -= point; // translate origin to point

    // Rotate
    double temp = x * cos(phi) - y * sin(phi); 
    y = x * sin(phi) + y * cos(phi); 
    x = temp;

    *this += point; // translate origin back

    return *this;
}

// Cylindrical coords
double Cartesian2Vector::r() const {return this->modulus();}
double Cartesian2Vector::phi() const {return atan2(y, x);}

// Create from cyl coords
Cartesian2Vector c2vFromRPhi(double r, double phi) {
C2V temp;
    temp.x = r * cos(phi);
    temp.y = r * sin(phi);
    return temp;
}

std::ostream & operator<< (std::ostream &s, const Cartesian2Vector &v) {
    return s << '(' << v.x << ", " << v.y << ")";
}


//
//    Return point of intersection of two lines. Lines join p1 to p2 and p3 to p4.
//    Ref Wikipedia Line line intersection.
//

int lineLine(C2V a1, C2V b1, C2V a2, C2V b2, C2V &p) {
/*    
 * Assume a1 != b1 and a2 != b2 and lines not parallel.
 * Line 1: p1 = a1 + lambda1(b1 - a1) 
 * Line 2: p2 = a2 + lambda1(b2 - a2)
 * These intersect when p1 = p2
 * To eleiminate lambda 2, we dot both sides with a vector perp. to line 2, namely R(pi/2)(a2 - b2)
 * This gives lambda1 = (a2 - a1) dot R(pi/2)(b2 - a2) / (b1 - a1) dot R(Pi/2)(b2 - a2)
 * (A zero denominator means no intersection: parallel, or one or both lines has a = b  
 */
    C2V perp = (b2 - a2).rotate(C2V(0., 0.), M_PI / 2.);
    double denom = (b1 - a1) * perp;
    if (fabs(denom) < 1.e-20) // Would be better to normalise...
        return 0;
    double lambda1 = (a2 - a1) * perp / denom;
    p = a1 + lambda1 * (b1 - a1);
    return 1;
}

int circLine(C2V center, double radius, C2V a, C2V b, C2V &p1, C2V &p2) {
/*
 * In frame centred at circle, the circle equation is pc^2 = r^2
 * Line is pl = a + lambda (b - a)
 * At intersections, these are same point, pl^2 = pc^2
 * Gives
 *    a^2 + lambda^2(b - a)^2 + 2 lambda a dot (b - a) - r^2 = 0
 * Take A = (b - a)^2; B = 2 a dot (b - a); C = a^2 - r^2 gives upto 2 values for lambda
 * Feed back into line equation to get the points. 
 */
    double lambda;
    if (radius < 0.0) return 0;
// Work in frame centred at the circle:
    a -= center;
    b -= center;
// Get coefficients
    double A = (b - a).squareModulus();
    double B = 2 * a * (b - a);
    double C = a.squareModulus() - radius * radius;
    double surd = B * B - 4. * A * C;
    if (surd < 0.0) return 0;
    if (surd == 0.0) {
        lambda = -B / 2. / A;
        p1 = a + lambda * (b - a);
        p1 += center;
        return 1;
    } 
    lambda = (-B + sqrt(surd)) / 2. / A;
    p1 = a + lambda * (b - a);
    lambda = (-B - sqrt(surd)) / 2. / A;
    p2 = a + lambda * (b - a);
// Convert back to user's frame
    p1 += center;
    p2 += center;
    return 2;
}

//
//   Cartesian 2-vectors that can be added, scaled, sized, dot and vector products etc.
//
#ifndef CARTESIAN2VECTOR_H
#define CARTESIAN2VECTOR_H

#include <ostream>

class Cartesian2Vector {
public:
double x, y;
    Cartesian2Vector (double xval = 0.0, double yval = 0.0): 
         x(xval), y(yval) {};

    Cartesian2Vector operator- (void);                     // Unary minus

    friend Cartesian2Vector operator+ (Cartesian2Vector a, Cartesian2Vector b); // Sum
    Cartesian2Vector & operator+= (Cartesian2Vector b);

    friend Cartesian2Vector operator- (Cartesian2Vector a, Cartesian2Vector b); // Difference
    Cartesian2Vector & operator-= (Cartesian2Vector b);

    friend double operator^ (Cartesian2Vector a, Cartesian2Vector b); // Outer products; return
    double operator^= (Cartesian2Vector b);                      // vector length ("z coord")

    friend double           operator* (Cartesian2Vector a, Cartesian2Vector b); // Inner product

    friend Cartesian2Vector operator* (Cartesian2Vector a, double scalefactor); // Post-scaling
    Cartesian2Vector & operator*= (double scalefactor);

    friend Cartesian2Vector operator/ (Cartesian2Vector a, double scalefactor); // Post-scaling by reciprocal (division)
    Cartesian2Vector & operator/= (double scalefactor);

    friend Cartesian2Vector operator* (double scalefactor, Cartesian2Vector a); // Pre-scaling
//    Cartesian2Vector & operator*= (double scalefactor);

    double modulus (void) const;
    double squareModulus (void) const;
    Cartesian2Vector & rotate(Cartesian2Vector point, double angle);
    double r() const;
    double phi() const;
    friend std::ostream & operator<< (std::ostream &s, const Cartesian2Vector &v);
};

// Abbreviation:
#define C2V Cartesian2Vector

// Useful functions

// Polar to Cartesian
Cartesian2Vector c2vFromRPhi(double r, double phi); 

// Find crossing point of a line and circle. Circle given by centre point and radius; line by any two points on it.
// Returns number of crossings (0, 1 or 2). Fills the crossing points into the last two parameters (if they exist)
int circLine(C2V circleCenter, double circleRadius, C2V point1OnLine, C2V point2OnLine, 
             C2V &firstIntersect, C2V &secondIntersect);

// Find crossing point of two lines. returns 0 for no crossing (parallel lines), 1 for one crossing, 2 for same line "infinit 
// xings", if found, crossing point is returned in last parameter.
int lineLine(C2V line1p1, C2V line1p2, C2V line2p1, C2V line2p2, C2V &crossing);

#endif // CARTESIAN2VECTOR_H
